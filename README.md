## Preface 
There are lots of limitations to this script as this is intended to be user friendly and automate most used tasks. For more generic usage, checkout http://scm.ia55.net/users/mehtasan/repos/kctl/browse.

## Usage
```kpod <apod> <app/kpod> <action>```

Examples:

~~~
kpod jupiter seed ls
kpod jupiter seed-0 jconsole
kpod jupiter seed-0 tdump
~~~

## Available actions
~~~
- ls - Lists the kpods for given apod and application
- jconsole - Opens jconsole
- desc - Describe 
- logs - less logs
- events - Display events 
- bash - Open bash session in the kpod
- version - Version of the application running (artifactService/getArtifactVersion)
- ip - ip of the kpod
- delete - Deletes the kpod
- kill - Sends kill 1 command to the kpod. 
- tdump - Takes a thread dump and sends it to your terminal. Pipe and save it in your local!
~~~

## Installation
```git clone https://gitlab.com/arcesium-rajendrn/kpod.git```

Install dateutil and pytz external libraries
~~~
pip3 install python-dateutil
pip3 install pytz
~~~

Add the below line in ~/.bashrc/.zshrc

~~~
alias kpod="<absolute_path_to_repo>/kpod"
source ~/.bashrc
Example: alias kpod="/home/rajendrn/repos/kpod/kpod"

chmod 700 kpod # This is must
~~~

## An apod is missing
No problem!. Add the pod to the cluster it belongs it to in the file
~~~

context_to_apod = {
    'prod--us-east-1--prod': ['baam', 'mio', 'tengyue', 'baly', 'boothbay', 
        'brdreach', 'brkfld', 'cert-manager', 'default', 'demo', 'deshaw', 
        'dragon', 'ejf', 'firtree', 'firtree-ubor', 'gatekeeper-system', 
~~~

## Why?
- No need to refresh aws credentials because the script automatically does that for you :)
- No need to substitue cluster names whenever switching between apods.
